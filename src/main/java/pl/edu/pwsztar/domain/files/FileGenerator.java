package pl.edu.pwsztar.domain.files;

import pl.edu.pwsztar.domain.dto.FileDto;
import pl.edu.pwsztar.domain.dto.FileGeneratorDto;
import java.io.IOException;

public interface FileGenerator {

    FileGeneratorDto toTxt(FileDto fileDto) throws IOException;
}
